﻿# Strategic Regions in Western Europe

region_soruin = {
	capital_province = xaac617	#Ozgar
	map_color = { 80 210 108 }
	states = { STATE_YARUHOL STATE_FASHTUG STATE_OZGAR STATE_BRAMMYAR STATE_JIBIRAEN STATE_NOGRUD STATE_KARLUR_DARAKH }
}

region_effelai = {
	capital_province = x83d386	#Vinescape
	map_color = { 53 73 38 }
	states = {  STATE_MARUKHAN STATE_THE_MIDDANS STATE_SCREAMING_JUNGLE STATE_WESTERN_EFFELAI STATE_MUSHROOM_FOREST STATE_CENTRAL_EFFELAI STATE_EASTERN_EFFELAI STATE_SORFEN STATE_SEINAINE STATE_QUIET_ISLE }
}

region_lai_peninsula = {
	capital_province = x9d2480	#Blaedrine
	map_color = { 255 225 120 }
	states = { STATE_DALAINE STATE_DEEPSONG STATE_THALASARAN STATE_KIOHALEN STATE_KIINDTIR STATE_DAZINKOST STATE_REZANOAN STATE_BROAN_KEIR STATE_NUREL STATE_ARENEL STATE_NUR_ELIZNA STATE_NEWSHORE STATE_TIMBERNECK STATE_VRENDIN STATE_AMANTAIL STATE_LAIPOINT_ARCHIPELAGO STATE_WEST_TURTLEBACK STATE_EAST_TURTLEBACK }
}

region_amadia = {
	capital_province = xff5c47	#Munasport
	map_color = { 0 170 197 }
	states = { STATE_BRONRIN STATE_NUR_DAMENATH STATE_MUNASIN STATE_SILVEGOR STATE_CLAMGUINN STATE_URANCESTIR STATE_CALILVAR STATE_CYMBEAHN STATE_OOMU_NELIR STATE_IMELAINE STATE_HARENAINE STATE_ARANLAS }
}

# region_taychend = {
	# capital_province = xff5c47	#Munasport
	# map_color = { 200 200 50 }
	# states = {  }
# }

# region_devand = {
	# capital_province = xff5c47	#Munasport
	# map_color = { 101 135 162 }
	# states = {  }
# }

# region_alecand = {
	# capital_province = xff5c47	#Munasport
	# map_color = { 209 187 63 }
	# states = {  }
# }