
STATE_YYL_MOITSA = {
    id = 633
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x205808" "x4A88BD" "x808096" "xA29827" "xA3E003" "xAD1C34" "xB2F3EF" "xD1C50C" "xE99378" "xFEF78B" }
    impassable = { "xD1C50C" "xA3E003" }
    traits = { state_trait_valak_river }
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 20
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_logging = 13
        bg_coal_mining = 36
        bg_fishing = 9
        bg_whaling = 8
    }
    naval_exit_id = 3308 #Hokhos Sea
}

STATE_MOITSA = {
    id = 634
    subsistence_building = "building_subsistence_farms"
    provinces = { "x25969B" "x26CC62" "x66AA9A" "x7B895F" "x7E4519" "x8B860F" "xBE9540" }
    traits = { state_trait_valak_river }
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 50
    arable_resources = { bg_rye_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 23
        bg_coal_mining = 36
        bg_sulfur_mining = 8
    }
}

STATE_MOITSADHI = {
    id = 635
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0AF7F2" "x1110F1" "x451B88" "x73DF8D" "xDD9E4E" "xEA4715" }
    traits = { state_trait_valak_river }
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 70
    arable_resources = { bg_rye_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 19
        bg_coal_mining = 36
        bg_fishing = 8
    }
    naval_exit_id = 3310 #Kodarve Lake
}

STATE_SKURKHA_KYARD = {
    id = 636
    subsistence_building = "building_subsistence_farms"
    provinces = { "x11520C" "xF53F22" "xFBB3DC" }
    traits = { state_trait_valak_river }
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 47
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_tobacco_plantations bg_cotton_plantations }
    capped_resources = {
        bg_logging = 15
        bg_iron_mining = 21
        bg_coal_mining = 28
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 20
    }
}

STATE_OVTO_KIGVAL = {
    id = 637
    subsistence_building = "building_subsistence_farms"
    provinces = { "x076C61" "x29E4D4" "x79DEF2" "xA2795A" "xF83446" }
    traits = { state_trait_valak_river }
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 52
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_tobacco_plantations bg_cotton_plantations }
    capped_resources = {
        bg_logging = 11
        bg_lead_mining = 27
        bg_iron_mining = 15
        bg_coal_mining = 36
    }
}

STATE_KVAKEINOLBA = {
    id = 638
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0CA7A2" "x117D0F" "x1BED69" "x50C897" "xB5F43B" "xF17173" }
    traits = { state_trait_valak_river }
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 58
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_tobacco_plantations bg_cotton_plantations }
    capped_resources = {
        bg_logging = 7
        bg_iron_mining = 96
        bg_coal_mining = 48
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 10
        discovered_amount = 2
    }
}

STATE_DZIMOKLI = {
    id = 640
    subsistence_building = "building_subsistence_farms"
    provinces = { "x223F23" "x273CEC" "x32219E" "x386A9A" "x6ED756" "x754C44" "x9EB9C6" "xE69B1A" }
    traits = { state_trait_valak_river state_trait_dzimokli_coal_fields }
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 75
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_tobacco_plantations bg_cotton_plantations }
    capped_resources = {
        bg_logging = 5
        bg_lead_mining = 18
        bg_iron_mining = 33
        bg_coal_mining = 72
    }
}

STATE_FOIRAKHIAN = {
    id = 642
    subsistence_building = "building_subsistence_farms"
    provinces = { "x010DFE" "x8137E4" "x9D0AA1" "xAB8821" "xD0D23C" "xD57D54" }
    traits = {}
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 20
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_tobacco_plantations bg_cotton_plantations }
    capped_resources = {
        bg_logging = 4
        bg_sulfur_mining = 8
    }
}

STATE_SHEVROMRZGH = {
    id = 643
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1EE4FE" "x2E4F7C" "x6B6C70" "x8C97C4" "x992DD9" }
    traits = {}
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 52
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_tobacco_plantations bg_cotton_plantations }
    capped_resources = {
        bg_logging = 7
        bg_lead_mining = 12
    }
}

STATE_NAGLAIBAR = {
    id = 644
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1829CE" "x7001F8" "x8BB887" "xA64DE0" "xA6D587" "xBBAB57" "xF7F653" }
    traits = {}
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 55
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_tobacco_plantations bg_cotton_plantations }
    capped_resources = {
        bg_logging = 7
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 30
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 4
    }
}

STATE_UGHEABAR = {
    id = 645
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1367D7" "x43F7AB" "x8FF5DF" "xCCDFC4" "xFF09DF" }
    traits = {}
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 35
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_tobacco_plantations bg_cotton_plantations bg_opium_plantations }
    capped_resources = {
        bg_logging = 4
    }
}

STATE_CAUBHEAMEAS = {
    id = 647
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x462492" "x48981F" "x528FAF" "x8A8224" "xB88C88" }
    impassable = { "x48981F" }
    traits = {}
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 20
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_iron_mining = 24
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 30
    }
}

STATE_SERPENT_GIFT = {
    id = 646
    subsistence_building = "building_subsistence_farms"
    provinces = { "x01E59A" "x08B43A" "x0DF765" "x2186CA" "x294C95" "x34ADFB" "x3B2C66" "x59C3EF" "x624555" "x70FD0C" "x8378EE" "x936170" "xB89C1A" "xBD1779" }
    traits = { state_trait_egoirlust_river }
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 90
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_opium_plantations bg_silk_plantations bg_tobacco_plantations bg_cotton_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 4
        bg_iron_mining = 21
        bg_coal_mining = 60
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 20
    }
}

STATE_SARLUN_GOILUST = {
    id = 650
    subsistence_building = "building_subsistence_farms"
    provinces = { "x36358C" "x792C1D" "x7DAEF4" "x9F22DB" "xA5584A" "xD00C27" }
    traits = { state_trait_egoirlust_river }
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 34
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_tobacco_plantations bg_cotton_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 6
        bg_iron_mining = 21
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 60
    }
}

STATE_IRDU_AGEENEAS = {
    id = 648
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x489820" "x66C888" "xEDBC86" "xF5B1D4" }
    impassable = { "x489820" }
    traits = {}
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 18
    arable_resources = { bg_livestock_ranches bg_opium_plantations }
    capped_resources = {
        bg_sulfur_mining = 20
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 10
    }
}

STATE_GHANEERSP = {
    id = 649
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x1E84F7" "x4ABAEB" "x4EC226" "x5FC71A" "x66E7D9" "x7EA361" "x843645" "xB8A2D5" "xC81EA2" "xD89969" "xDB825D" "xDDE627" }
    traits = {}
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 74
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_tobacco_plantations bg_cotton_plantations bg_opium_plantations }
    capped_resources = {
        bg_logging = 2
        bg_lead_mining = 18
    }
}

STATE_SOCHULEAG = {
    id = 651
    subsistence_building = "building_subsistence_farms"
    provinces = { "x052AD3" "x0B8953" "x1A012A" "x1B88EF" "x4C3948" "x74A6A9" "x803344" "x9C7C3C" "x9EA06C" "xA4EEB5" "xAE80DC" "xB3155A" "xC5EFAA" "xC8C92C" "xEC338D" }
    traits = { state_trait_valak_river }
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 73
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_tobacco_plantations bg_cotton_plantations bg_silk_plantations bg_tea_plantations bg_opium_plantations bg_dye_plantations }
    capped_resources = {
        bg_logging = 20
        bg_fishing = 11
        bg_coal_mining = 28
    }
}

STATE_ADOI_FILEANAN = {
    id = 652
    subsistence_building = "building_subsistence_farms"
    provinces = { "x179BB0" "x2170F4" "x303390" "x4B05DC" "x5207B2" "xA540CB" "xD693AB" "xDE1BB6" "xE63DF2" "xFC7FD8" }
    traits = { state_trait_egoirlust_river }
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 106
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_tobacco_plantations bg_cotton_plantations bg_silk_plantations bg_tea_plantations bg_opium_plantations }
    capped_resources = {
        bg_logging = 7
        bg_fishing = 4
        bg_sulfur_mining = 40
    }
    naval_exit_id = 3310 #Kodarve Lake
}

STATE_ORCHEKH = {
    id = 653
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0A17D9" "x283AF1" "x4169BF" "x535535" "x5F7B45" "x6A69AA" "x7C4DD7" "x7C4E81" "x80E168" "x82AEEA" "xB8AC65" "xFA2B20" "xFD17FD" }
    traits = {}
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 80
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_tobacco_plantations bg_cotton_plantations bg_silk_plantations bg_tea_plantations bg_opium_plantations }
    capped_resources = {
        bg_logging = 5
        bg_fishing = 10
    }
    naval_exit_id = 3310 #Kodarve Lake
}

STATE_APORLAEN = {
    id = 654
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1122E1" "x2B625B" "x37F671" "x3B0176" "x5BC534" "x627FF9" "x82AAAD" "x90E34A" "x96C163" "xA56292" "xA58559" "xB4C583" "xB51C81" "xF2F77F" }
    traits = { state_trait_natural_harbors state_trait_kozyoshdek_river }
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 72
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_tobacco_plantations bg_cotton_plantations bg_silk_plantations bg_tea_plantations bg_opium_plantations }
    capped_resources = {
        bg_fishing = 9
        bg_logging = 4
        bg_iron_mining = 24
    }
    naval_exit_id = 3311 #Yukelqur Lake
}

STATE_ZARMIKLON = {
    id = 655
    subsistence_building = "building_subsistence_farms"
    provinces = { "x2511A2" "x261D9B" "x2863F6" "x297963" "x2C4366" "x3874D5" "x4D47BB" "x5D6F1E" "x8B6169" "xC6E2E7" "xFBB47F" }
    traits = {}
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 77
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_tobacco_plantations bg_cotton_plantations bg_opium_plantations }
    capped_resources = {
        bg_fishing = 7
        bg_logging = 6
        bg_iron_mining = 18
        bg_coal_mining = 12
        bg_sulfur_mining = 40
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 30
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 4
    }
    naval_exit_id = 3311 #Yukelqur Lake
}

STATE_BULREKAYIG = {
    id = 656
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0A4B4E" "x4A71B7" "x692DF6" "x702A8C" "x76F040" "x77ABFA" "x79CEFC" "xA85F09" "xBE5CDB" "xC7FD89" "xCB5B7E" "xD72502" "xE956E8" }
    traits = { state_trait_volutsabaj_river }
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 101
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_tobacco_plantations bg_cotton_plantations bg_silk_plantations bg_tea_plantations bg_opium_plantations }
    capped_resources = {
        bg_fishing = 9
        bg_logging = 7
        bg_iron_mining = 30
        bg_coal_mining = 12
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 5
    }
    naval_exit_id = 3313 #Zenruuk Lake
}

STATE_AKANDHIL = {
    id = 657
    subsistence_building = "building_subsistence_farms"
    provinces = { "x03F9E1" "x2CE750" "x485920" "x4AE046" "x517EF4" "x75D184" "x7E74AB" "x7F6449" "x815FD1" "x9F438F" "xA87FE4" "xC8483F" "xF1C98D" "xF85CF5" }
    traits = { state_trait_akandhil_mineral_fields state_trait_mudholkhin_river }
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 18
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_tobacco_plantations bg_cotton_plantations bg_opium_plantations }
    capped_resources = {
        bg_logging = 4
        bg_iron_mining = 21
        bg_coal_mining = 28
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 7
        discovered_amount = 1
    }
}

STATE_TOZGONREK = {
    id = 658
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0E0134" "x251EB0" "x48E840" "x5E8F10" "x88669F" "x933C2B" "xCBA1FA" "xE54467" "xE69A65" }
    traits = { state_trait_natural_harbors state_trait_mudholkhin_river }
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 74
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_silk_plantations bg_tea_plantations }
    capped_resources = {
        bg_fishing = 8
        bg_logging = 13
        bg_iron_mining = 45
        bg_coal_mining = 32
    }
    naval_exit_id = 3309 #Blue Sea
}

STATE_ZOITAL = {
    id = 659
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0646CE" "x0C080C" "x137B97" "x23227E" "x3CB2BD" "x47DFC6" "x48B486" "x48FE5F" "x6249B5" "x808DE6" "x8E1A24" "xA943B7" "xE02E1B" "xE8704B" }
    traits = {}
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 34
    arable_resources = { bg_rye_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 23
        bg_lead_mining = 12
        bg_iron_mining = 75
        bg_coal_mining = 36
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 8
    }
}

STATE_ZUURZOI = {
    id = 660
    subsistence_building = "building_subsistence_farms"
    provinces = { "x2909FD" "x298F9B" "x4DE223" "x53F284" "x5FE13B" "x61C91B" "x7DECD0" "xE37CF0" "xEF0E17" "xF72864" }
    traits = {}
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 62
    arable_resources = { bg_rye_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 9
        bg_whaling = 7
        bg_logging = 17
        bg_lead_mining = 21
        bg_iron_mining = 27
        bg_coal_mining = 32
        bg_sulfur_mining = 32
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 10
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 4
    }
    naval_exit_id = 3308 #Hokhos Sea
}

STATE_PRIKOYOL = {
    #canal place TODO
    id = 661
    subsistence_building = "building_subsistence_farms"
    provinces = { "x37895A" "x494EB5" "x4C0F9E" "x5533AC" "x614242" "x623DF1" "xE76B56" }
    traits = { state_trait_kalyins_gift }
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 142
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_tobacco_plantations bg_cotton_plantations bg_silk_plantations bg_tea_plantations bg_opium_plantations }
    capped_resources = {
        bg_fishing = 15
        bg_logging = 4
        bg_lead_mining = 10
        bg_sulfur_mining = 30
    }
    naval_exit_id = 3310 #Kodarve Lake
}

STATE_GADHLUMO = {
    id = 662
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0EB0AE" "x306177" "x5A61DF" "x6A26BD" "x8B4AC3" "xABD505" "xCC6BEA" "xE2CB30" "xFEFF38" }
    traits = { state_trait_kalyins_gift }
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 194
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_tobacco_plantations bg_silk_plantations bg_tea_plantations bg_dye_plantations }
    capped_resources = {
        bg_fishing = 14
        bg_logging = 9
        bg_iron_mining = 15
        bg_coal_mining = 10
        bg_sulfur_mining = 35
    }
    naval_exit_id = 3310 #Kodarve Lake
}

STATE_PEENADHI = {
    id = 663
    subsistence_building = "building_subsistence_farms"
    provinces = { "x025279" "x2C3458" "x377F84" "x435DCE" "x925CA5" "xD4AB21" "xDED078" "xE6763F" }
    traits = { state_trait_kalyins_gift state_trait_natural_harbors }
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 146
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_tobacco_plantations bg_silk_plantations bg_tea_plantations bg_dye_plantations }
    capped_resources = {
        bg_fishing = 8
        bg_logging = 5
        bg_iron_mining = 42
        bg_coal_mining = 6
        bg_sulfur_mining = 32
    }
    naval_exit_id = 3310 #Kodarve Lake
}

STATE_YUKARON = {
    id = 664
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1EADAB" "x39E063" "x49F7B7" "x4C9B67" "x55B68D" "x6BE172" "x9F41D8" "xC06F20" "xF930B0" }
    traits = { state_trait_kalyins_gift }
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 191
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_tobacco_plantations bg_silk_plantations bg_tea_plantations bg_dye_plantations }
    capped_resources = {
        bg_fishing = 9
        bg_logging = 7
        bg_lead_mining = 32
        bg_coal_mining = 5
        bg_sulfur_mining = 22
    }
    naval_exit_id = 3311 #Yukelqur Lake
}

STATE_YUKAROYOL = {
    id = 665
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0A322C" "x65DE80" "x98C01A" "xC6833A" "xD73827" "xDD811B" "xFBC49D" }
    traits = { state_trait_kalyins_gift }
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 149
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_tobacco_plantations bg_silk_plantations bg_tea_plantations }
    capped_resources = {
        bg_fishing = 10
        bg_logging = 20
        bg_lead_mining = 30
        bg_iron_mining = 5
        bg_coal_mining = 4
        bg_sulfur_mining = 10
    }
    naval_exit_id = 3311 #Yukelqur Lake
}

STATE_QUSHYIL = {
    id = 666
    subsistence_building = "building_subsistence_farms"
    provinces = { "x139F9F" "x7C881B" "x9B1750" "xA268F8" "xB4AB17" "xDD7863" }
    traits = { state_trait_kalyins_gift }
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 117
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_tobacco_plantations bg_silk_plantations bg_tea_plantations }
    capped_resources = {
        bg_fishing = 7
        bg_logging = 3
        bg_iron_mining = 10
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 32
    }
    naval_exit_id = 3312 #Oriolg Channel
}

STATE_KESH_GOLKHIN = {
    id = 667
    subsistence_building = "building_subsistence_farms"
    provinces = { "x266F9C" "x29755D" "x58D24E" "x5E9CD0" "x976C2E" "xA1F1AA" "xBA64F0" "xC8C1D7" "xD4DB0B" }
    traits = { state_trait_kalyins_gift }
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 249
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_silk_plantations bg_tea_plantations }
    capped_resources = {
        bg_fishing = 12
        bg_logging = 5
        bg_iron_mining = 45
        bg_coal_mining = 11
    }
    naval_exit_id = 3313 #Zenruuk Lake
}

STATE_SARTZ_NEISAR = {
    #btw this needs to be a canal place TODO
    id = 639
    subsistence_building = "building_subsistence_farms"
    provinces = { "x522332" "x677497" "x7DCD58" "x865426" "xDDB6AC" "xF3EB35" }
    traits = { state_trait_kalyins_gift }
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 146
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_tea_plantations }
    capped_resources = {
        bg_logging = 8
        bg_fishing = 13
        bg_coal_mining = 8
        bg_lead_mining = 24
    }
    naval_exit_id = 3309 #Blue Sea
}

STATE_SHIKENKHIIN = {
    id = 641
    subsistence_building = "building_subsistence_farms"
    provinces = { "x3439FE" "x35599F" "x6E690E" "x726217" "x893833" "xC414E5" }
    traits = { state_trait_kalyins_gift state_trait_ultakal_mines }
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 222
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_silk_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 4
        bg_whaling = 5
        bg_fishing = 7
        bg_coal_mining = 25
        bg_gold_mining = 4
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 2
    }
    naval_exit_id = 3313 #Zenruuk Lake
}

STATE_ZOI_KHORKHIIN = {
    id = 668
    subsistence_building = "building_subsistence_farms"
    provinces = { "x5712AF" "x651B87" "x9D7AF7" "xED4750" "xF351AD" }
    traits = { state_trait_kalyins_gift }
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 101
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_tea_plantations }
    capped_resources = {
        bg_logging = 15
        bg_whaling = 7
        bg_fishing = 12
        bg_iron_mining = 19
        bg_coal_mining = 5
        bg_lead_mining = 21
    }
    naval_exit_id = 3309 #Blue Sea
}

STATE_SHIK_DAZAR = {
    id = 669
    subsistence_building = "building_subsistence_farms"
    provinces = { "x059E60" "x301E6D" "x39B4EC" "x3BDFA7" "x799B74" "xAA0916" "xB46A85" "xD6383C" "xED28D0" "xF4F267" }
    traits = { state_trait_kalyins_gift }
    city = "xD7C642"
    farm = "xD7C642"
    wood = "xD7C642"
    arable_land = 146
    arable_resources = { bg_rye_farms bg_livestock_ranches bg_silk_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 8
        bg_whaling = 8
        bg_fishing = 12
        bg_coal_mining = 7
        bg_lead_mining = 45
    }
    naval_exit_id = 3309 #Blue Sea
}

